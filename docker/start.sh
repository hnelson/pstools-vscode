#!/usr/bin/env bash
set -o errexit -o nounset -o pipefail
IFS=$'\n\t\v'

git clone https://gitlab.cern.ch/hnelson/l1-hlt-prescales.git 
code-server l1-hlt-prescales
