package main

import (
	"encoding/json"
	"encoding/xml"
	"io/ioutil"
	"l1xml2json/internal/types"
	"os"
	"path/filepath"
	"strings"

	"github.com/sirupsen/logrus"
)

func main() {
	if len(os.Args) == 1 {
		logrus.Fatal("no Level-1 XML file given as argument")
	}
	xmlPath, err := filepath.Abs(os.Args[1])
	if err != nil {
		logrus.WithError(err).WithField("path", xmlPath).Fatal("could not read xml file")
	}
	logrus.WithField("path", xmlPath).Info("reading XML file")

	bytes, err := ioutil.ReadFile(xmlPath)
	if err != nil {
		logrus.WithError(err).WithField("path", xmlPath).Fatal("could not read xml file")
	}

	var l1xml types.L1XML
	err = xml.Unmarshal(bytes, &l1xml)
	if err != nil {
		logrus.WithError(err).WithField("path", xmlPath).Fatal("could not parse xml file")
	}

	var prescales types.L1XMLPrescaleTable
	for _, p := range l1xml.Contexts[0].Params {
		if p.ID == "prescales" {
			prescales.Columns = p.Columns
			prescales.Rows = p.Rows
			prescales.Types = p.Types
			break
		}
	}

	j := types.L1JSON{}
	jprescales := map[string]types.L1JSONAlgo{}
	for _, p := range prescales.Rows.Rows {
		jprescales[p.AlgoName] = types.L1JSONAlgo{Columns: p.Values}
	}
	j.Prescales.Algos = jprescales

	jbytes, err := json.MarshalIndent(j, "", "  ")
	if err != nil {
		logrus.WithError(err).Fatal("could not generate JSON")
	}

	jsonPath, err := filepath.Abs(strings.TrimSuffix(xmlPath, filepath.Ext(xmlPath)) + ".json")
	if err != nil {
		logrus.WithError(err).Fatal("could not write JSON")
	}
	logrus.WithField("path", jsonPath).Info("writing file")
	err = ioutil.WriteFile(jsonPath, jbytes, os.ModePerm)
	if err != nil {
		logrus.WithError(err).Fatal("could not write JSON")
	}

	columns := []types.JSONColumn{}
	for _, column := range prescales.Columns.Columns {
		columns = append(columns, types.JSONColumn{Name: column})
	}

	columnsPath, err := filepath.Abs(filepath.Dir(xmlPath) + "/columns.json")
	if err != nil {
		logrus.WithError(err).Fatal("could not generate columns JSON")
	}
	cbytes, err := json.MarshalIndent(types.ColumnsJSON{Columns: columns}, "", "  ")
	if err != nil {
		logrus.WithError(err).Fatal("could not generate columns JSON")
	}
	logrus.WithField("path", columnsPath).Info("writing file")
	err = ioutil.WriteFile(columnsPath, cbytes, os.ModePerm)
	if err != nil {
		logrus.WithError(err).Fatal("could not write columns JSON")
	}

}
