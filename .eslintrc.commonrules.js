module.exports = {
  "no-console": [
    process.env.CI === undefined ? "warn" : "error",
    { allow: ["warn", "error"] },
  ],
  "no-debugger": process.env.CI === undefined ? "warn" : "error",
  "sort-imports": [
    "warn",
    {
      ignoreCase: true,
      ignoreDeclarationSort: true,
      ignoreMemberSort: true,
    },
  ],
  "@typescript-eslint/no-use-before-define": [
    "error",
    { functions: false, classes: true, typedefs: false },
  ],
  "no-unused-vars": ["warn", { argsIgnorePattern: "^_" }],
  "@typescript-eslint/no-unused-vars": ["warn", { argsIgnorePattern: "^_" }],
  curly: "warn",
  eqeqeq: "warn",
  "no-throw-literal": "warn",
  semi: ["warn", "always"],
  quotes: ["warn", "double"],
  "quote-props": ["warn", "as-needed"],
  // "comma-dangle": "off", // extended by @typescript-eslint/comma-dangle
  // "@typescript-eslint/comma-dangle": ["warn", {
  "comma-dangle": [
    "warn",
    {
      arrays: "always-multiline",
      objects: "always-multiline",
      imports: "always-multiline",
      exports: "always-multiline",
      functions: "always-multiline",
      // "enums": "always-multiline",
      // "generics": "always-multiline",
      // "tuples": "always-multiline"
    },
  ],
  "space-before-function-paren": ["warn", {
    anonymous: "never",
    named: "never",
    asyncArrow: "always",
  }],
};