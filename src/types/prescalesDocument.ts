import { L1Prescales } from "./l1Prescales";

export interface PrescalesDocument<T extends L1Prescales> {
  prescales: T;
}
