import { onMounted, onUnmounted } from "vue";
import { Message, MessageTypesMap } from "../../../../../types/messages";

interface VSCode {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  postMessage(message: any): void;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  getState(): Record<string, any>;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  setState(state: Record<string, any>): void;
}

declare function acquireVsCodeApi(): VSCode;

const vscode = acquireVsCodeApi();

export function sendMessage<T extends keyof MessageTypesMap>(message: Message<T>): void {
  vscode.postMessage(JSON.stringify(message));
}

type MessageFn<T extends keyof MessageTypesMap> = (m: Message<T>) => void;

interface Handler<T extends keyof MessageTypesMap> {
  type: T;
  cb: MessageFn<T>;
}

const handlers: Handler<keyof MessageTypesMap>[] = [];

export function addMessageHandler<T extends keyof MessageTypesMap>(
  type: T,
  cb: MessageFn<keyof MessageTypesMap>,
): () => void {
  const handler: Handler<keyof MessageTypesMap> = { type, cb };
  handlers.push(handler);
  return () => {
    const i = handlers.indexOf(handler as Handler<keyof MessageTypesMap>);
    if (i !== -1) {
      handlers.splice(i, 1);
    }
  };
}

window.addEventListener("message", event => {
  const message = event.data; // The json data that the extension sent
  const t = message.type as keyof MessageTypesMap;
  const parsedMessage = message as Message<keyof MessageTypesMap>;
  handlers.filter(h => h.type === t).forEach(h => h.cb(parsedMessage));
});

export function vueMessageHandler<T extends keyof MessageTypesMap>(
  type: T,
  fn: (message: Message<keyof MessageTypesMap>) => void,
): void {
  // eslint-disable-next-line @typescript-eslint/no-empty-function
  let dispose = () => {};

  onMounted(() => {
    dispose = addMessageHandler(type, fn);
  });

  onUnmounted(() => {
    dispose();
  });
}
