import { Uri } from "vscode";

export const siblingUri = (parentUri: Uri, filename: string): Uri => {
  const p = parentUri.path.split("/");
  p.pop();
  p.push(filename + ".json");

  return parentUri.with({ path: p.join("/") });
};
