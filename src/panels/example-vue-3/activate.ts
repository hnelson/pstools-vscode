import * as vscode from "vscode";
import { TableEditor } from "./tableEditor";

// register commands declared in package.json
export function activate(context: vscode.ExtensionContext): void {
  context.subscriptions.push(
    vscode.commands.registerCommand("pstools.opentableeditor", async () => {
      let doc = vscode.window.activeTextEditor?.document.uri;
      if (!doc?.path.endsWith("l1.json")) {
        doc = undefined;
      }

      if (!doc && vscode.workspace.workspaceFolders) {
        for (const w of vscode.workspace.workspaceFolders) {
          const uri = vscode.Uri.joinPath(w.uri, "l1.json");
          try {
            vscode.workspace.fs.stat(uri);
            doc = uri;
            // eslint-disable-next-line no-empty
          } catch (e) {}
        }
      }

      if (!doc) {
        vscode.window.showInformationMessage("Open the desired pstools file first to open its table editor");
        return;
      }
      TableEditor.create(context.extensionPath, doc);
    }),
  );

  vscode.window.registerWebviewPanelSerializer(TableEditor.viewType, {
    async deserializeWebviewPanel(panel: vscode.WebviewPanel, state: Record<string, string>) {
      TableEditor.revive(panel, context.extensionPath, vscode.Uri.parse(state.uri, true));
    },
  });
}
