module.exports = {
  outputDir: "build",
  filenameHashing: false,
  configureWebpack: {
    performance: {
      // we serve a locally hosted app, so we can go pretty far
      // with asset file sizes, but fail a build when it gets ridiculous
      hints: "error",
      // individual asset size
      maxAssetSize: 5242880, // 5MiB
      // total size
      maxEntrypointSize: 31457280 // 30MiB
      // assetFilter: function(assetFilename) {
      //   // Function predicate that provides asset filenames
      //   return assetFilename.endsWith(".css") || assetFilename.endsWith(".js");
      // }
    },
    optimization: {
      splitChunks: {
        chunks: "all"
      }
    },
    devtool: "source-map"
  },
  chainWebpack: config => {
    config.module
      .rule("eslint")
      .use("eslint-loader")
      .options({
        fix: true
      });
  }
};
