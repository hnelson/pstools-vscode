import * as path from "path";
import * as vscode from "vscode";

import { getNonce } from "../../extension/getNonce";

interface MessageTypeMap {
  message: {
    message: string;
  };
  fileContent: {
    uri: string;
    content?: string;
    error?: Error;
  }[];
  command: Command<keyof CommandTypeMap>;
}
export type MessageType = keyof MessageTypeMap;

interface CommandTypeMap {
  getFileContent: {
    names: string[];
  };
}
export type CommandType = keyof CommandTypeMap;

export interface Command<T extends CommandType> {
  name: T;
  data: CommandTypeMap[T];
}

export interface Message<T extends keyof MessageTypeMap> {
  type: T;
  payload: MessageTypeMap[T];
}

export function makeMessage<T extends MessageType>(type: T, payload: MessageTypeMap[T]): Message<T> {
  return {
    type,
    payload,
  };
}

export class TableEditor {
  public static panels: TableEditor[] = [];

  public static readonly viewType = "pstools-table-editor";

  private readonly panel: vscode.WebviewPanel;
  private readonly extensionRoot: string;
  private disposables: vscode.Disposable[] = [];
  private sourceDocument: vscode.Uri;

  public static getScriptsRoot(extensionRoot: string): string {
    return path.join(extensionRoot, "out", "panels", "table-editor", "panelcode", "build");
  }

  public static create(extensionRoot: string, uri: vscode.Uri): void {
    const textEditorColumn = vscode.window.activeTextEditor?.viewColumn;

    const panel = vscode.window.createWebviewPanel(
      this.viewType,
      "PSTools Table Editor",
      textEditorColumn || vscode.ViewColumn.One,
      {
        // allow (sandboxed) javascript to run in the panel
        enableScripts: true,
        // restrict the webview to only loading content from our extension's `panelcode` directory.
        localResourceRoots: [vscode.Uri.file(this.getScriptsRoot(extensionRoot))],
      },
    );

    this.revive(panel, extensionRoot, uri);
  }

  public static revive(panel: vscode.WebviewPanel, extensionRoot: string, uri: vscode.Uri): void {
    this.panels.push(new TableEditor(panel, extensionRoot, uri));
  }

  private constructor(panel: vscode.WebviewPanel, extensionRoot: string, uri: vscode.Uri) {
    this.panel = panel;
    this.extensionRoot = extensionRoot;
    this.sourceDocument = uri;

    // Set the webview's initial html content
    this.renderHTML();

    // Listen for panel is disposal
    // This happens when the user closes the panel or when the panel is closed programmatically
    this.panel.onDidDispose(() => this.dispose(), this, this.disposables);

    // when the panels internal state changes
    this.panel.onDidChangeViewState(() => this.panel.visible && this.renderHTML(), this, this.disposables);

    // when receiving a message from the panels javascript code
    this.panel.webview.onDidReceiveMessage(this.handleMessage, this, this.disposables);

    // act when source text document changed
    vscode.workspace.onDidChangeTextDocument(
      (e: vscode.TextDocumentChangeEvent) => {
        if (e.document.fileName.endsWith("l1.json") || e.document.fileName.endsWith("columns.json")) {
          this.panel.webview.postMessage(
            makeMessage("fileContent", [
              {
                content: e.document.getText(),
                uri: e.document.uri.toString(),
              },
            ]),
          );
        }
      },
      null,
      this.disposables,
    );
  }

  private async handleMessage(message: Message<MessageType>) {
    if (message.type === "command") {
      return this.handleCommand(message as Message<"command">);
    } else if (message.type === "fileContent") {
      return this.handleFileContent(message as Message<"fileContent">);
    }
  }

  private async handleCommand(message: Message<"command">) {
    const payload = message.payload;
    if (payload.name === "getFileContent") {
      const names = (payload as Command<"getFileContent">).data.names;

      if (names.length === 0) {
        names.push(this.sourceDocument.toString());
        names.push(vscode.Uri.joinPath(this.sourceDocument, "../columns.json").toString());
      }

      const result: Message<"fileContent">["payload"] = [];

      for (const name of names) {
        const uri = vscode.Uri.parse(name, true);
        try {
          const doc = await vscode.workspace.openTextDocument(uri);
          const content = doc.getText();
          result.push({
            uri: name,
            content,
          });
        } catch (error) {
          result.push({
            uri: name,
            error,
          });
        }
      }

      return this.panel.webview.postMessage(makeMessage("fileContent", result));
    }
  }

  private async handleFileContent(message: Message<"fileContent">) {
    const payloads = message.payload;
    for (const payload of payloads) {
      const uri = vscode.Uri.joinPath(this.sourceDocument, "..", payload.uri);

      vscode.workspace.fs.writeFile(uri, new TextEncoder().encode(payload.content));
    }
  }

  private renderHTML() {
    this.panel.title = "PSTools Table Editor";

    const rootfolder = this.panel.webview.asWebviewUri(vscode.Uri.file(TableEditor.getScriptsRoot(this.extensionRoot)));
    const nonce = getNonce();
    const html = `<!DOCTYPE html>
<html lang=en>
  <head>
    <meta charset=utf-8>
    <meta http-equiv=X-UA-Compatible content="IE=edge">
    <meta name=viewport content="width=device-width,initial-scale=1">
    <link rel=icon href=/favicon.ico>
    <title>table-editor</title>
    <link nonce="${nonce}" href=${rootfolder}/css/app.css rel=preload as=style>
    <link nonce="${nonce}" href=${rootfolder}/js/app.js rel=preload as=script>
    <link nonce="${nonce}" href=${rootfolder}/js/chunk-vendors.js rel=preload as=script>
    <link nonce="${nonce}" href=${rootfolder}/css/app.css rel=stylesheet>
  </head>
  <body rooturl="${rootfolder}"><noscript><strong>We're sorry but table-editor doesn't work properly without JavaScript enabled. Please enable it
        to continue.</strong></noscript>
    <div id=app></div>
    <script nonce="${nonce}" src=${rootfolder}/js/chunk-vendors.js></script>
    <script nonce="${nonce}" src=${rootfolder}/js/app.js></script>
  </body>
</html>`;
    this.panel.webview.html = html;
  }

  public dispose(): void {
    const i = TableEditor.panels.indexOf(this);
    if (i !== -1) {
      TableEditor.panels.splice(i, 1);
      this.panel.dispose();
      this.disposables.forEach(d => d.dispose());
      this.disposables = [];
    }
  }
}
