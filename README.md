# PSTools Visual Studio Code extension

## Compiling & running

This project is a VSCode extension, you can run it by opening this repository using your installation of VSCode.

Run `make`, and then press `F5` in your VSCode editor.

Note: although `F5` also builds the extension code like `make` does, this project contains webview panels, which has front-end code that needs to be built as well.
If you don't intend on altering front-end code (panel UIs), you can run `make` once and only run `F5` after.